var Statsbar = React.createClass({
render: function() {
  return (
  	<div className="stats-bar">
	 	<SingleValueStat label="Scheduled Patients" value="7" />
		<SingleValueStat label="Active Patients" value="45"/>
		<SingleValueStat label="Success rate" value="93" type="percentage"/>	
		<SingleValueStat label="Total loadings" value="2998"/>	


  	</div>

  )
}
});

var SingleValueStat = React.createClass({
render: function() {
	this.value = <div className="value">{this.props.value}</div>
	if(this.props.type =="percentage") this.value = <div className="value">{this.props.value} <span className="trailing">%</span></div>
	if(this.props.type =="days") this.value = <div className="value">{this.props.value} <span className="trailing">DAYS</span></div>	
  return (
	<div className="single-value-stat">
		{this.value}
		<div className="label">
			{this.props.label}
		</div>
	</div>
  )
}
});

ReactDOM.render(
	<Statsbar/>,
	document.getElementById('stats-bar-wrapper')
);